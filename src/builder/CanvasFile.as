package builder
{
	import flash.events.DataEvent;
	import flash.events.Event;
	import flash.events.IEventDispatcher;
	import flash.net.FileFilter;
	import flash.net.FileReference;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	import flash.utils.describeType;
	
	import flashx.textLayout.elements.Configuration;

	public class CanvasFile
	{
		private static var fileRef:FileReference;
		
		private static const NONE:String = "none";
		private static const SAVE:String = "save";
		private static const LOAD:String = "load";
		private static var status:String = NONE;
		
		public function CanvasFile()
		{
		}
		
		public static function save():void
		{
			var saveFile:XML = generateSaveFile(Game.builder.canvas);
			
			fileRef = new FileReference();
			fileRef.save(saveFile, ".txt");
			status = SAVE;
			configureListeners(fileRef);
		}
		
		private static function generateSaveFile(canvas:Canvas):XML
		{
			var xml:XML = <canvas />
			
			for each (var obj:CanvasObject in canvas.objects)
			{
				var object:XML = <object/>
					
				var start:XML=<new/>
				start.setName("start");
				start.appendChild(obj.start);
				object.appendChild(start);
				
				var end:XML=<new/>
				end.setName("end");
				end.appendChild(obj.end);
				object.appendChild(end);
				
				var attributes:XML=<new/>
				attributes.setName("attributes");
				for (var attr:String in obj.attributes)
				{
					var attribute:XML = <new/>
					attribute.setName(attr);
					attribute.appendChild(obj.attributes[attr]);
					attributes.appendChild(attribute);
				}
				object.appendChild(attributes);
				
				xml.appendChild(object);
			}
				
			return xml;
		}
		
		public static function load():void
		{
			fileRef = new FileReference();
			configureListeners(fileRef);
			fileRef.browse([new FileFilter("Canvas Text Files (*.txt)", "*.txt")]);
			status = LOAD;
		}
		
		private static function loadFromSaveFile(file:FileReference):void
		{
			var xml:XML = XML(file.data);
			trace(xml);
			
		}
		
		private static function configureListeners(dispatcher:IEventDispatcher):void 
		{
			dispatcher.addEventListener(Event.CANCEL, cancelHandler);
			dispatcher.addEventListener(Event.COMPLETE, completeHandler);
			dispatcher.addEventListener(Event.OPEN, openHandler);
			dispatcher.addEventListener(Event.SELECT, selectHandler);
			dispatcher.addEventListener(DataEvent.UPLOAD_COMPLETE_DATA,uploadCompleteDataHandler);
		}
		
		private static function cancelHandler(event:Event):void {
			trace("cancelHandler: " + event);
		}
		
		private static function completeHandler(event:Event):void {
			trace("completeHandler: " + event);
			var file:FileReference = FileReference(event.target);
			loadFromSaveFile(file);
		}
		
		private static function uploadCompleteDataHandler(event:DataEvent):void {
			trace("uploadCompleteData: " + event);
		}
		
		private static function openHandler(event:Event):void {
			trace("openHandler: " + event);
		}
		
		private static function selectHandler(event:Event):void {
			var file:FileReference = FileReference(event.target);
			trace("selectHandler: name = " + file.name);
			if (status == LOAD)
				file.load();
		}
		
	}
}
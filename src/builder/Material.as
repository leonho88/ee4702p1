package builder
{
	import sound.EQ;
	
	/*
	Material
	A static class that allows us to obtain color, or filter properties given a material ID
	related classes EQ.as
	*/
	public class Material
	{
		public static const CONCRETE:int = 0;
		public static const WOOD:int = 1;
		
		private static function doorTransmit(tempEq:EQ):void
		{
			tempEq.lllg = 1;
			tempEq.llg = 0.655;
			tempEq.lg = 0.264;
			tempEq.mg = 0.08;
			tempEq.hg = 0.069;
			tempEq.hhg = 0.069;
			tempEq.hhhg = 0.092;
		}
		
		private static function doorReflect(tempEq:EQ):void
		{
			tempEq.lllg = 0;
			tempEq.llg = 0;
			tempEq.lg = 0;
			tempEq.mg = 0;
			tempEq.hg = 0.046;
			tempEq.hhg = 0.402;
			tempEq.hhhg = 1;
		}
		
		private static function concreteReflect(tempEq:EQ):void
		{
			tempEq.lllg = 0.997;
			tempEq.llg = 0.997;
			tempEq.lg = 0.933;
			tempEq.mg = 0.933;
			tempEq.hg = 0.920;
			tempEq.hhg = 0.908;
			tempEq.hhhg = 0.92;
		}
		private static function concreteTransmit(tempEq:EQ):void
		{
			tempEq.lllg = 0.103;
			tempEq.llg = 0.103;
			tempEq.lg = 0.067;
			tempEq.mg = 0.067;
			tempEq.hg = 0.080;
			tempEq.hhg = 0.092;
			tempEq.hhhg = 0.08;
		}
		
		private static function glassReflect(tempEq:EQ):void
		{
			tempEq.lllg = 0;
			tempEq.llg = 0.046;
			tempEq.lg = 0.103;
			tempEq.mg = 0.195;
			tempEq.hg = 0.425;
			tempEq.hhg = 0.770;
			tempEq.hhhg = 1;
		}
		private static function glassTransmit(tempEq:EQ):void
		{
			tempEq.lllg = 0.609;
			tempEq.llg = 0.356;
			tempEq.lg = 0.264;
			tempEq.mg = 0.172;
			tempEq.hg = 0.126;
			tempEq.hhg = 0.092;
			tempEq.hhhg = 0.069;
		}
		
		public static function getReverbFilterForMaterial(type:int):EQ
		{
			var eq:EQ;
			switch (type)
			{
				case CONCRETE:
					eq = new EQ();
					concreteReflect(eq);
					break;
				case WOOD:
					eq = new EQ();
					doorReflect(eq);
					break;
				default:
					break;
			}
			return eq;
		}
		
		public static function getTransmitFilterForMaterial(type:int):EQ
		{
			var eq:EQ;
			switch (type)
			{
				case CONCRETE:
					eq = new EQ();
					concreteTransmit(eq);
					break;
				case WOOD:
					eq = new EQ();
					doorTransmit(eq);
					break;
				default:
					break;
			}
			return eq;
		}
		
		public static function getColorForMaterial(type:int):uint
		{
			var retVal:uint;
			switch (type)
			{
				case CONCRETE:
					retVal = 0x666666;
					break;
				case WOOD:
					retVal = 0xCC6600;
					break;
				default:
					break;
			}
			return retVal;
		}
	}
}
// author : Leon Ho
package builder
{
	import flash.display.Sprite;
	import flash.events.KeyboardEvent;
	import flash.geom.Matrix3D;
	import flash.geom.Point;
	import flash.geom.Vector3D;
	import flash.ui.Keyboard;
	
	import sound.AudioSource;
	
	/*
	CanvasSound
	An object that produces sound
	related classes AudioSource
	*/
	public class CanvasSound extends Sprite implements IUpdateable, IDrawable, ICanvasObject
	{
		private var _selected:Boolean = false;
		public function get selected():Boolean
		{
			return _selected;
		}
		public function set selected(val:Boolean):void
		{
			_selected = val;
		}	
		
		private var _attributes:Object = new Object();
		public function get attributes():Object
		{
			return _attributes;
		}
		
		private var canvas:Canvas;
		
		public var audioSource:AudioSource;
		
		public function CanvasSound(canvas:Canvas, position:Point)
		{
			super();
			
			this.canvas = canvas;
			
			this.x = position.x;
			this.y = position.y;
			
			// spatial attributes
			_attributes.position = new Vector3D(position.x, 0, position.y);
			_attributes.facing = new Vector3D(0, 0, 1);
			_attributes.renderColor = 0xFF9900;
			
			// angle needs to be in radians
			_attributes.innerAngle = 60;
			_attributes.outerAngle = 160;
			
			// sound attributes
			_attributes.sourceGain = 1.0;
			_attributes.rolloff_factor = 3.0;
			_attributes.reference_distance = 1.0; // distance at which the listener will experience the gain
			_attributes.max_distance = 20.0; // distance beyond which the source will no longer be attenuated
			
			_attributes.looping = 0;
			_attributes.active = 0;
			
			audioSource = new AudioSource(this);
			
			this.addEventListener(KeyboardEvent.KEY_UP, onKeyUp);
		}
		
		private function onKeyUp(e:KeyboardEvent):void
		{
			if (e.keyCode == Keyboard.BACKSPACE || e.keyCode == Keyboard.DELETE)
			{
				canvas.removeCanvasSound(this);
				canvas.selectedObject = null;
			}
			else if (e.keyCode == Keyboard.ESCAPE)
			{
				canvas.selectedObject = null;
			}
		}
		
		public function rotateTo(x:int, z:int):void
		{
			var to:Vector3D = new Vector3D(x, 0, z);
			_attributes.facing = to.subtract(_attributes.position);
			(_attributes.facing as Vector3D).normalize();
		}
	
		public function update(deltaTime:Number):void
		{
			_attributes.position.x = this.x;
			_attributes.position.z = this.y;
		}
		
		public function draw(deltaTime:Number):void
		{
			graphics.clear();
			var radius:int = 8;
			graphics.lineStyle();
			graphics.beginFill(_attributes.renderColor);
			graphics.drawCircle(0, 0, radius);
			graphics.endFill();
			graphics.lineStyle(3, 0xFFFF00);
			graphics.moveTo(0, 0);
			graphics.lineTo(_attributes.facing.x*radius, _attributes.facing.z*radius);
			graphics.lineStyle(1);
			// draw sound cone
			var coneRadius:int = 16;
			// outer cone
			graphics.moveTo(0, 0);
			var innerAngleRad:Number = (_attributes.innerAngle/180)*Math.PI;
			var outerAngleRad:Number = (_attributes.outerAngle/180)*Math.PI;
			var startAngle:Number = Math.atan2(_attributes.facing.z, _attributes.facing.x) - outerAngleRad*0.5;
			graphics.lineTo((Math.cos(startAngle)*coneRadius), Math.sin(startAngle)*coneRadius);
			drawArc(0, 0, startAngle, startAngle + outerAngleRad, coneRadius, 1);
			graphics.lineTo(0, 0);
			// inner cone
			coneRadius *= 2;
			startAngle = Math.atan2(_attributes.facing.z, _attributes.facing.x) - innerAngleRad*0.5;
			graphics.lineTo((Math.cos(startAngle)*coneRadius), Math.sin(startAngle)*coneRadius);
			drawArc(0, 0, startAngle, startAngle + innerAngleRad, coneRadius, 1);
			graphics.lineTo(0,0);
		}
		
		private function drawArc(centerX:Number, centerY:Number, startAngle:Number, endAngle:Number, radius:Number, direction:Number):void
			/* 
			centerX  -- the center X coordinate of the circle the arc is located on
			centerY  -- the center Y coordinate of the circle the arc is located on
			startAngle  -- the starting angle to draw the arc from
			endAngle    -- the ending angle for the arc
			radius    -- the radius of the circle the arc is located on
			direction   -- toggle for going clockwise/counter-clockwise
			*/
		{
			var difference:Number = Math.abs(endAngle - startAngle);
			/* How "far" around we actually have to draw */
			
			var divisions:Number = Math.floor(difference / (Math.PI / 4))+1;
			/* The number of arcs we are going to use to simulate our simulated arc */
			
			var span:Number    = direction * difference / (2 * divisions);
			var controlRadius:Number    = radius / Math.cos(span);
			
			graphics.moveTo(centerX + (Math.cos(startAngle)*radius), centerY + Math.sin(startAngle)*radius);
			var controlPoint:Point;
			var anchorPoint:Point;
			for(var i:Number=0; i<divisions; ++i)
			{
				endAngle    = startAngle + span;
				startAngle  = endAngle + span;
				
				controlPoint = new Point(centerX+Math.cos(endAngle)*controlRadius, centerY+Math.sin(endAngle)*controlRadius);
				anchorPoint = new Point(centerX+Math.cos(startAngle)*radius, centerY+Math.sin(startAngle)*radius);
				graphics.curveTo(
					controlPoint.x,
					controlPoint.y,
					anchorPoint.x,
					anchorPoint.y
				);
			}
		}
	}
}
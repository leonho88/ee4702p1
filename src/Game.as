// author : Leon Ho
package
{
	import builder.Builder;
	
	import flash.display.Sprite;
	import flash.display.StageAlign;
	import flash.display.StageScaleMode;
	import flash.display3D.*;
	import flash.events.Event;
	import flash.utils.*;
	
	[SWF(width="1315", height="600", frameRate="60", backgroundColor="#FFFFFF")]
	public class Game extends Sprite
	{
		// timing
		private const TICKS_PER_SECOND:int = 30;
		private const TICK_RATE:Number = 1.0 / TICKS_PER_SECOND;
		private const TICK_RATE_MS:Number = TICK_RATE * 1000;
		private const MAX_TICKS_PER_FRAME:int = 5;
		
		private var lastTime:Number = 0;
		private var accumulator:Number = 0;
		
		// graphics context
		public var context3D:Context3D;
		
		// fps
		public static var fps:Number = 0.0;
		private var frames:uint = 0;
		private var frameAccumulator:Number = 0;
		
		// audio
		public static var audioLatency:int = 0;
		
		// objects
		public var updateables:Vector.<IUpdateable> = new Vector.<IUpdateable>;
		public var drawables:Vector.<IDrawable> = new Vector.<IDrawable>;
		
		public static var builder:Builder;
		public static var console:Console;
		
		// visualization
		public static var visual:Visualization;

		public function Game()
		{
			stage.stage3Ds[0].addEventListener(Event.CONTEXT3D_CREATE, init);
			stage.stage3Ds[0].requestContext3D();
			
			stage.align = StageAlign.TOP_LEFT;
			stage.scaleMode = StageScaleMode.NO_SCALE;
			
			addEventListener(Event.ENTER_FRAME, onFrame);
		}
		
		private function init(e:Event):void
		{
			context3D = stage.stage3Ds[0].context3D;
			context3D.configureBackBuffer(800, 600, 2, true);
			
			// the world builder
			builder = new Builder(this);
			addUpdateable(builder);
			addDrawable(builder);
			
			// dev console
			console = new Console(this);
			addUpdateable(console);
			
			// author : Arun
			visual = new Visualization(800, 100, 515, 250);
			addChild(visual);
		}
		
		private function addUpdateable(obj:IUpdateable):void
		{
			updateables.push(obj);
		}
		
		private function addDrawable(obj:IDrawable):void
		{
			drawables.push(obj);
		}
		
		private function onFrame(e:Event):void
		{
			// Track current time.
			var currentTime:Number = getTimer();
			if (lastTime < 0)
			{
				lastTime = currentTime;
				return;
			}
			
			// Calculate time since last frame and advance that much.
			var deltaTime:Number = Number(currentTime - lastTime);
			advance(deltaTime);
			
			// Note new last time.
			lastTime = currentTime;
			
			visual.enabled = true;
		}
		
		private function advance(deltaTime:Number):void
		{
			accumulator += deltaTime;
			
			var loopCount:int = 0;
			// update loop
			while (accumulator >= TICK_RATE_MS && loopCount < MAX_TICKS_PER_FRAME)
			{
				updateAll(TICK_RATE);
				
				accumulator -= TICK_RATE_MS;
				loopCount++;
			}
			
			context3D.clear(1, 1, 1, 1);
			
			// render loop
			drawAll(deltaTime);
			
			frames++; // number of frames drawn
			frameAccumulator += deltaTime; // time taken
			if (frameAccumulator >= 1000) // every second
			{
				fps = frames; // frames per second
				frames = 0; // reset frame count
				frameAccumulator -= 1000; // remove second from accumulator
			}
			
			context3D.present();
		}
		
		private function updateAll(deltaTime:Number):void
		{
			for (var i:uint; i < updateables.length; i++)
			{
				updateables[i].update(deltaTime);
			}
		}
		
		private function drawAll(deltaTime:Number):void
		{
			for (var i:uint; i < drawables.length; i++)
			{
				drawables[i].draw(deltaTime);
			}
		}
	}
}
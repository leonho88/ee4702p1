// author : Leon Ho
package sound
{
	import builder.CanvasSound;
	
	import flash.events.Event;
	import flash.media.Sound;
	import flash.net.URLRequest;
	
	/*
	AudioSource
	An object owned by a CanvasSound object that holds the sound object, from which mp3 data can be extracted
	It can be extended to handle loading in a more asynchronous manner, but we have not done so
	related classes CanvasSound.as
	*/
	public class AudioSource
	{
		public var source:Sound = new Sound();
		public var soundLoaded:Boolean = false;
		
		public var owner:CanvasSound;
		
		public function AudioSource(owner:CanvasSound)
		{
			this.owner = owner;
		}
		
		public function loadSound(file:String):void
		{
			source.load(new URLRequest(file));
			source.addEventListener(Event.COMPLETE, onLoadComplete);
		}
		
		public function onLoadComplete(e:Event):void
		{
			soundLoaded = true;
		}
	}
}
package utils
{
	import builder.CanvasObject;
	
	import flash.geom.Point;

	public class IntersectionInfo
	{
		public var intersection:Point; // point of intersection
		public var object:CanvasObject; // the object being intersected
		
		public function IntersectionInfo()
		{
		}
	}
}
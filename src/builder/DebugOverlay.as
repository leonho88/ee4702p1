// author : Leon Ho
package builder
{
	import flash.display.Sprite;
	import flash.geom.Point;
	import flash.geom.Vector3D;
	
	import utils.CanvasUtils;
	import utils.IntersectionInfo;
	
	/*
	DebugOverlay
	An overlay on our canvas to show debug information
	related classes Canvas.as, CanvasUtils.as, IntersectionInfo.as 
	*/
	public class DebugOverlay extends Sprite implements IUpdateable, IDrawable
	{
		private var canvas:Canvas;
		
		private const INTERSECTION_POINT_COLOR:uint = 0xFF0000;
		private const LINE_COLOR:uint = 0x990099;
		private const VIRTUAL_SRC_ALPHA:Number = 0.5;
		
		public function DebugOverlay(canvas:Canvas)
		{
			super();
			
			this.canvas = canvas;
			
			this.mouseEnabled = false;
		}
		
		public function update(deltaTime:Number):void
		{
		}
		
		public function draw(deltaTime:Number):void
		{
			graphics.clear();
			
			if (!Canvas.debug) return;
			
			var listenerPoint:Point = new Point(canvas.avatar.attributes.position.x, canvas.avatar.attributes.position.z);
			var sourcePoint:Point = new Point();
			
			for each (var snd:CanvasSound in canvas.sounds)
			{
				sourcePoint.x = snd.attributes.position.x;
				sourcePoint.y = snd.attributes.position.z;
				var intersections:Vector.<IntersectionInfo> = CanvasUtils.castRay(sourcePoint, listenerPoint, canvas.objects);
				// if source unobstructed to listener
				if (intersections.length == 0)
				{
					// draw info for reverb
					for each (var obj:CanvasObject in canvas.objects)
					{
						var virtualPoint:Point = CanvasUtils.reflectPointInLine(sourcePoint, obj.start, obj.end);
						// cast ray to see if virtual ray intersects wall
						var virtualRayIntersections:Vector.<IntersectionInfo> = CanvasUtils.castRay(listenerPoint, virtualPoint, canvas.objects);
						// if it intersects, we reflect the source in the first canvas object hit, then we draw the information
						if (virtualRayIntersections.length >= 1 && obj == virtualRayIntersections[0].object)
						{
							// draw virtual source
							var virtualFacing:Vector3D = new Vector3D();
							virtualFacing.x = -snd.attributes.facing.x;
							virtualFacing.y = -snd.attributes.facing.z;
							virtualFacing.z= snd.attributes.facing.y;
							virtualFacing.normalize();
							
							var objdirection:Vector3D = CanvasUtils.direct(obj.end, obj.start);
							virtualFacing = CanvasUtils.reflect3D(objdirection, virtualFacing);
							
							var radius:int = 8;
							graphics.beginFill(snd.attributes.renderColor, VIRTUAL_SRC_ALPHA);
							graphics.drawCircle(virtualPoint.x, virtualPoint.y, radius);
							graphics.endFill();
							graphics.lineStyle(3, 0xFFFF00);
							graphics.moveTo(virtualPoint.x, virtualPoint.y);
							graphics.lineTo(virtualPoint.x + virtualFacing.x*radius, virtualPoint.y + virtualFacing.y*radius);
							graphics.lineStyle();
							
							// draw line from listener to wall
							graphics.lineStyle(1, LINE_COLOR);
							graphics.moveTo(listenerPoint.x, listenerPoint.y);
							graphics.lineTo(virtualRayIntersections[0].intersection.x, virtualRayIntersections[0].intersection.y);
							graphics.lineTo(sourcePoint.x, sourcePoint.y);
							graphics.lineStyle();
						}
					}
				}
				for each (var info:IntersectionInfo in intersections)
				{
					// draw line from source to listener
					graphics.lineStyle(1, LINE_COLOR);
					graphics.moveTo(sourcePoint.x, sourcePoint.y);
					graphics.lineTo(listenerPoint.x, listenerPoint.y);
					graphics.lineStyle();
					
					// draw intersection point
					graphics.beginFill(INTERSECTION_POINT_COLOR);
					graphics.drawCircle(info.intersection.x, info.intersection.y, 3);
					graphics.endFill();
				}
			}
		}
	}
}
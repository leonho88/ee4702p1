//author : Arun
//This class created a FFT visualizer to aid in debug mode
package  {
	import flash.display.Shape;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.media.SoundMixer;
	import flash.utils.ByteArray;		

	public class Visualization extends Sprite {
		
		private var line : Shape;
		private var _enabled:Boolean;
		// Varible that stores the width of the Visualization window
		private var _width:int;
		// Varible that stores the height of the Visualization window
		private var _height:int;
		// Varible that stores the position x of the Visualization window
		private var _posx:int;
		// Varible that stores the position y of the Visualization window
		private var _posy:int;
		
		public function Visualization(posx:int,posy:int, width:int, height:int) 
		{
			super();
			
			line = new Shape( );
			addChild( line );
			
			//Draw reference line
			graphics.beginFill( 0x000000 );
			graphics.drawRect( posx, posy, width, height );
			graphics.lineStyle( 1, 0x222222 );
			graphics.moveTo( posx, (height + 1)+posy );
			graphics.lineTo( width+posx, (height + 1)+posy );
			
			//assign variable values
			_width = width;
			_height = height;
			_posx = posx;
			_posy = posy;
		}
		
		private function enterFrameHandler(event:Event):void 
		{
			var ba : ByteArray = new ByteArray( );
			//Use inbuilt function to simulate the Visualization window
			SoundMixer.computeSpectrum( ba, false );
			var off : Number = 0;
			var i : int = 0;
			//Draw the line graphics for the points in the Visualization window
			with ( line.graphics )
			{
				clear( );
				moveTo( _posx, (_height >> 1)+_posy );
				lineStyle( 1, 0xEEEEFF, 0.6 );
				for ( i = 0; i < 512; i++ )
				{
					if ( i >= 0xFF && off == 0 )
					{
						off = _width;
						lineTo( (_width+_posx), (_height >> 1)+_posy );
						lineStyle( 3, 0xEEEEFF, 0.2 );
						moveTo( _posx, _posy );
					}
					lineTo( (i / 256 * _width - off)+_posx, (-ba.readFloat() * 125 + (_height >> 1))+_posy );
				}
			}
		}
		
		public function set enabled(enabled:Boolean):void
		{
			_enabled = enabled;
			if (_enabled) {
				addEventListener(Event.ENTER_FRAME, enterFrameHandler);
			} else {
				removeEventListener(Event.ENTER_FRAME, enterFrameHandler);
			}
		}
		
		public function get enabled():Boolean
		{
			return _enabled;
		}
		
	}
}
// author : Leon Ho
package builder
{
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.KeyboardEvent;
	import flash.events.MouseEvent;
	import flash.geom.Point;
	import flash.geom.Vector3D;
	import flash.ui.Keyboard;
	
	/*
	CanvasObject
	The walls/doors in our world, objects that can interact with sound
	related classes Material.as
	*/
	public class CanvasObject extends Sprite implements IUpdateable, IDrawable, ICanvasObject
	{
		public var start:Point;
		public var end:Point;
		
		private var canvas:Canvas;
		
		private var _selected:Boolean = false;
		public function get selected():Boolean
		{
			return _selected;
		}
		public function set selected(val:Boolean):void
		{
			_selected = val;
		}
		
		private var hovered:Boolean = false;
		
		// attributes object (maps string keys to values)
		private var _attributes:Object = new Object();
		public function get attributes():Object
		{
			return _attributes;
		}
		
		private var renderThickness:Number = 0;
		
		public function CanvasObject(canvas:Canvas, start:Point, end:Point)
		{
			this.start = start;
			this.end = end;
			
			this.canvas = canvas;
			
			_attributes.position = Point.interpolate(start, end, 0.5); // default to midpoint
			_attributes.rotation = Vector3D.angleBetween(new Vector3D(end.x-start.x, end.y-start.y, 0), Vector3D.X_AXIS); // rotation from x-axis in radians
			_attributes.height = 3.5; // temporary constant height in m
			_attributes.thickness = 3; // default thickness
			_attributes.material = 0;
			var vector:Point = end.subtract(start);
			_attributes.vector = new Vector3D(vector.x, 0, vector.y);
			_attributes.length = _attributes.vector.length * canvas.ptm_ratio;

			this.addEventListener(KeyboardEvent.KEY_UP, onKeyUp);
		}
		
		private function onKeyUp(e:KeyboardEvent):void
		{
			if (e.keyCode == Keyboard.BACKSPACE || e.keyCode == Keyboard.DELETE)
			{
				canvas.removeCanvasObject(this);
				canvas.selectedObject = null;
			}
			else if (e.keyCode == Keyboard.ESCAPE)
			{
				canvas.selectedObject = null;
			}
		}
		
		private function onRollOver(e:MouseEvent):void
		{
			hovered = true;
		}
		
		private function onRollOut(e:MouseEvent):void
		{
			hovered = false;
		}
		
		public function getRenderThickness():Number
		{
			if (hovered || _selected)
				return _attributes.thickness + 1;
			else
				return _attributes.thickness;
		}
		
		public function getRenderColor():Number
		{
			return Material.getColorForMaterial(_attributes.material);
		}
		
		public function update(deltaTime:Number):void
		{
		}
		
		public function draw(deltaTime:Number):void
		{
			this.graphics.clear();
			
			this.graphics.lineStyle(this.getRenderThickness(), this.getRenderColor());
			this.graphics.moveTo(start.x, start.y);
			this.graphics.lineTo(end.x, end.y);
			this.graphics.lineStyle();
		}
	}
}
// author : Leon Ho
package builder
{
	import flash.display.InteractiveObject;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.KeyboardEvent;
	import flash.events.MouseEvent;
	import flash.geom.Point;
	import flash.ui.Keyboard;
	
	import utils.CanvasUtils;
	
	public class Canvas extends Sprite implements IUpdateable, IDrawable
	{
		// pixel to meters ratio
		public function get ptm_ratio():Number
		{
			return 1.0/attributes.ptm;
		}
		
		// grid color
		private const color_inactive:uint = 0x696969;
		private const color_hover:uint = 0x000000;
		
		// canvas attributes
		public var attributes:Object = new Object();
		
		// mouse interaction
		private var mousePos:Point = new Point();
		private var trackMouse:Boolean = false;
		
		private var points:Vector.<Point> = new Vector.<Point>();
		private var startPoint:Point = new Point();
		private var endPoint:Point = new Point();
		
		// object vectors
		// walls
		public var objects:Vector.<CanvasObject> = new Vector.<CanvasObject>();
		// sounds
		public var sounds:Vector.<CanvasSound> = new Vector.<CanvasSound>();
		// player avatar
		public var avatar:CanvasAvatar;
		
		// reference for selected context
		public var selectedObject:ICanvasObject;
		
		// simulation and debug
		public static var grid:Boolean = true;
		public static var reverb:Boolean = true;
		public static var occlusion:Boolean = true;
		public static var debug:Boolean = true;
		public static var debugOverlay:DebugOverlay;
		
		public function Canvas()
		{
			super();
			
			// size of the canvas
			attributes.size = 512;
			
			// level of points on canvas
			attributes.granularity = 8;
			
			// pixel to meters
			attributes.ptm = 32;
			
			// avatar
			avatar = new CanvasAvatar(this);
			addChild(avatar);
			avatar.x = attributes.size * 0.5;
			avatar.y = attributes.size * 0.5;
			
			// canvas interactivity
			this.addEventListener(MouseEvent.MOUSE_MOVE, onMouseMove);
			this.addEventListener(MouseEvent.MOUSE_DOWN, onMouseDown);
			this.addEventListener(MouseEvent.MOUSE_UP, onMouseUp);
			this.addEventListener(MouseEvent.CLICK, select);
			
			// debug
			debugOverlay = new DebugOverlay(this);
			addChild(debugOverlay);
			
			this.addEventListener(Event.ADDED, onAdd);
		}
		
		private function onAdd(e:Event):void
		{
			stage.addEventListener(KeyboardEvent.KEY_DOWN, onKeyDown);
			stage.addEventListener(KeyboardEvent.KEY_UP, onKeyUp);
			
			this.removeEventListener(Event.ADDED, onAdd);
		}
		
		private function onKeyDown(e:KeyboardEvent):void
		{
			// move
			if (e.keyCode == Keyboard.W)
			{
				e.stopImmediatePropagation();
				avatar.moveForward();
			}
			else if (e.keyCode == Keyboard.S)
			{
				e.stopImmediatePropagation();
				avatar.moveBackward();
			}
		}
		
		private function onKeyUp(e:KeyboardEvent):void
		{
			if (e.keyCode == Keyboard.W || e.keyCode == Keyboard.S)
			{
				avatar.stop();
			}
		}
		
		private function onMouseMove(e:MouseEvent):void
		{
			var half_gran:int = attributes.granularity * 0.5;
			// snap to point
			mousePos.x = (uint)((e.localX + half_gran)/attributes.granularity)*attributes.granularity;
			mousePos.y = (uint)((e.localY + half_gran)/attributes.granularity)*attributes.granularity;
		
			avatar.rotateTo(e.stageX, e.stageY);
			
			if (selectedObject is CanvasSound)
			{
				(selectedObject as CanvasSound).rotateTo(e.stageX, e.stageY);
			}
		}
		
		private function onMouseDown(e:MouseEvent):void
		{
			if (e.target == avatar || e.target is CanvasSound)
			{
				e.stopImmediatePropagation();
				(e.target as Sprite).startDrag(false, getRect(this));
			}
			else
			{
				trackMouse = true;
				
				startPoint.x = mousePos.x;
				startPoint.y = mousePos.y;
			}

		}
		
		private function onMouseUp(e:MouseEvent):void
		{	
			if (e.target == avatar || e.target is CanvasSound)
			{
				e.stopImmediatePropagation();
				(e.target as Sprite).stopDrag();
			}
			else
			{
				if (!endPoint.equals(startPoint))
					addCanvasObject(startPoint, endPoint);
				
				trackMouse = false;
			}
		}
		
		public function addCanvasSound(position:Point, file:String):void
		{
			var newSound:CanvasSound = new CanvasSound(this, position);
			newSound.audioSource.loadSound(file);
			sounds.push(newSound);
			addChild(newSound);
		}
		
		public function removeCanvasSound(snd:CanvasSound):void
		{
			sounds.splice(sounds.indexOf(snd), 1);
			removeChild(snd);
			snd.removeEventListener(MouseEvent.CLICK, select);
		}
		
		public function addCanvasObject(start:Point, end:Point):void
		{
			var newObject:CanvasObject = new CanvasObject(this, start.clone(), end.clone());
			objects.push(newObject);
			addChild(newObject);
		}
		
		public function removeCanvasObject(obj:CanvasObject):void
		{
			objects.splice(objects.indexOf(obj), 1);
			removeChild(obj);
			obj.removeEventListener(MouseEvent.CLICK, select);
		}
		
		private function select(e:MouseEvent):void
		{
			if (e.target is ICanvasObject)
			{
				if (selectedObject) selectedObject.selected = false;
				selectedObject = e.target as ICanvasObject;
				selectedObject.selected = true;

				(selectedObject as InteractiveObject).focusRect = false;
				stage.focus = (selectedObject as InteractiveObject);
			}
		}
		
		public function update(deltaTime:Number):void
		{
			// track the mouse position with snapping
			if (trackMouse)
			{
				endPoint.x = mousePos.x;
				endPoint.y = mousePos.y;
			}
			
			// update all objects in the scene
			for each (var obj:CanvasObject in objects)
			{
				obj.update(deltaTime);
			}
			for each (var snd:CanvasSound in sounds)
			{
				snd.update(deltaTime);
			}
			avatar.update(deltaTime);
			
			swapChildren(avatar, getChildAt(numChildren-1));
			swapChildren(debugOverlay, getChildAt(numChildren-1));
		}
		
		public function draw(deltaTime:Number):void
		{
			this.graphics.clear();
			
			// draw world bounds
			this.graphics.lineStyle(1);
			this.graphics.beginFill(0xFFFFFF, 1.0);
			this.graphics.drawRect(0, 0, attributes.size, attributes.size);
			this.graphics.endFill();
			this.graphics.lineStyle();
			
			// draw the grid dots
			if (grid)
			{
				for (var dx:int = 0; dx < attributes.size; dx += attributes.granularity)
				{
					for (var dy:int = 0; dy < attributes.size; dy += attributes.granularity)
					{
						this.graphics.beginFill(color_inactive);
						this.graphics.drawCircle(dx, dy, 1.0);
						this.graphics.endFill();
					}
				}
			}
			
			// draw line for line creation
			if (trackMouse)
			{
				this.graphics.lineStyle(1);
				this.graphics.moveTo(startPoint.x, startPoint.y);
				this.graphics.lineTo(endPoint.x, endPoint.y);
				this.graphics.lineStyle();
			}
			
			// draw all objects in the scene
			for each (var obj:CanvasObject in objects)
			{
				obj.draw(deltaTime);
			}
			for each (var snd:CanvasSound in sounds)
			{
				snd.draw(deltaTime);
			} 
			avatar.draw(deltaTime);
			
			debugOverlay.draw(deltaTime);
		}
	}
}
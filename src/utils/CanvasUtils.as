// author : Leon Ho
package utils
{
	import builder.CanvasObject;
	
	import flash.geom.Point;
	import flash.geom.Vector3D;
	
	/*
	CanvasUtils
	*/

	public class CanvasUtils
	{
		// cast a ray from a point to another
		// returns the canvas objects that intersects with the ray, in order
		public static function castRay(from:Point, to:Point, objects:Vector.<CanvasObject>):Vector.<IntersectionInfo>
		{
			var results:Vector.<IntersectionInfo> = new Vector.<IntersectionInfo>();
			var intersectingObjects:Vector.<CanvasObject> = new Vector.<CanvasObject>();
			// test each CanvasObject for intersections, store them in IntersectionInfos
			for each (var obj:CanvasObject in objects)
			{
				var intersect:Point = intersectLineWithLineSegment(from, to, obj.start, obj.end);
				if (intersect)
				{
					intersectingObjects.push(obj);
					var info:IntersectionInfo = new IntersectionInfo();
					info.intersection = intersect;
					info.object = obj;
					results.push(info);
				}
			}
			// if a ray intersects multiple objects, perform a recursive subdivision to get the order of intersection
			if (intersectingObjects.length > 1)
			{
				var midpoint:Point = Point.interpolate(from, to, 0.5);
				var near:Vector.<IntersectionInfo> = castRay(from, midpoint, intersectingObjects);
				var far:Vector.<IntersectionInfo> = castRay(midpoint, to, intersectingObjects);
				for (var i:int = 0; i < near.length; i++)
				{
					results[i] = near[i];
				}
				for (i = 0; i < far.length; i++)
				{
					results[near.length + i] = far[i];
				}
			}
			return results;
		}
		
		public static function intersectLineWithLineSegment(from:Point, to:Point, p0:Point, p1:Point):Point
		{
			// the result is the intersection point
			var result:Point;
			
			var r:Number;
			var s:Number;
			var d:Number;
			//Make sure the lines aren't parallel
			if ((to.y - from.y) / (to.x - from.x) != (p1.y - p0.y) / (p1.x - p0.x))
			{
				d = (((to.x - from.x) * (p1.y - p0.y)) - (to.y - from.y) * (p1.x - p0.x));
				if (d != 0)
				{
					r = (((from.y - p0.y) * (p1.x - p0.x)) - (from.x - p0.x) * (p1.y - p0.y)) / d;
					s = (((from.y - p0.y) * (to.x - from.x)) - (from.x - p0.x) * (to.y - from.y)) / d;
					if (r >= 0 && r <= 1)
					{
						if (s >= 0 && s <= 1)
						{
							result = new Point(from.x + r * (to.x - from.x), from.y + r * (to.y - from.y));
						}
					}
				}
			}
			return result;
		}
		
		
		public static function distanceFromLine(p2:Point, p0:Point, p1:Point):Number
		{
			return reflectPointInLine(p2, p0, p1).subtract(p2).length * 0.5;
		}
		
		public static function reflectPointInLine(p2:Point, p0:Point, p1:Point):Point
		{
			var nx:Number = p1.x - p0.x;
			var ny:Number = p1.y - p0.y;
			var d:Number  = Math.sqrt(nx*nx + ny*ny);
			nx /= d;
			ny /= d;
			
			var px:Number = p2.x - p0.x;
			var py:Number = p2.y - p0.y;
			var w:Number  = nx*px + ny*py;
			var rx:Number = 2*p0.x - p2.x + 2*w*nx;
			var ry:Number = 2*p0.y - p2.y + 2*w*ny;
			
			return new Point(rx, ry);
		}
		
		// authors : Arun, Chun Kit
		public static function reflect3D(wall_vect:Vector3D, source_vect:Vector3D):Vector3D
		{
			var ret_vect:Vector3D = new Vector3D();
			var d:Number;
			d = source_vect.dotProduct(wall_vect) * 2;
			ret_vect.x = source_vect.x - (d * wall_vect.x);
			//due to Leon's convention of using z axis
			ret_vect.y = source_vect.y - (d * wall_vect.y);
			ret_vect.normalize();
			return ret_vect;
		}
		
		//This function returns the Vector3D given 2 points
		public static function direct(start:Point,end:Point):Vector3D
		{
			var temp:Point = new Point();
			//not sure Leon's convention, but this works for us 
			temp = end.subtract(start);
			var direction:Vector3D = new Vector3D(temp.x,temp.y,0,0);
			direction.normalize();
			return direction;
		}
		
		//This function returns the distace between 2 points
		public static function retDistance(start:Point, end:Point):Number
		{
			var retdistace:Number = 0;
			var x:Number;
			var y:Number;
			x = start.x - end.x;
			y = start.y - end.y;
			retdistace = Math.sqrt(x*x + y*y);
			return retdistace;
		}
	}
}
// ActionScript file


package sound {
	
	public class delay {
		
		//----------------------------------------------------		-----------------
		//
		//  Variables
		//
		//----------------------------------------------------
		
		private var _count:Number = 2;
		private var _feedback : Number;
		private var _useMilliseconds : Boolean;
		private var _length : int;
		private var _mix : Number;

		private var invMix : Number;
		
		private var readPointer : int = 0;
		private var writePointer : int = 0;
		private var delayValue : Number;
		
		private var buffer : Vector.<Number>;
		
		private var channelCopy : delay;
		
		private var repeat:Number;
		//----------------------------------------------------		-----------------
		//
		//  Constructor
		//
		//----------------------------------------------------	-----------------
			//feedback = reduction of image gain	//length = delay time	
		public function delay( feedback : Number = 0.2, length : int = 5000, mix : Number = 0.7, useMilliseconds : Boolean = true ) {
			_feedback = feedback;
			_useMilliseconds = useMilliseconds;
			this.length = length;
			_mix = mix;
		}
		
		
		//----------------------------------------------------		-----------------
		//
		//  Public methods
		//
		//----------------------------------------------------		-----------------

		public function process(input : Number, num_times:Number = 0) : Number {		
			
			readPointer = writePointer - _length + 	1;
			if ( readPointer < 0 ) readPointer += _length;
			
			//trace ("readpointer: " + 	readPointer);
			
			delayValue = buffer[ readPointer ];
			
			//trace ("delayValue: " + delayValue);
			
			//Delays only ONE time by the _length
			buffer[ writePointer ] = input * ( 1 - _feedback );// + delayValue * _feedback;
				
				//trace ("buffer[" + writePointer + "]: " + buffer[writePointer]);
				
				if ( ++writePointer == _length ) writePointer = 0;
				
				return delayValue * _mix;//input * ( 1 - _mix ) + delayValue * _mix;
			

		}
		
		public function duplicate() : delay {
			var l : Number = _length;
			if ( _useMilliseconds ) l /= 44100 * 0.001;
			channelCopy = new delay( _feedback, l, _mix, _useMilliseconds );
			return channelCopy;
		}
		
		public function get feedback() : Number {
			return _feedback;
		}
		
		public function set feedback(feedback : Number) : void {
			_feedback = feedback;
			if ( channelCopy ) channelCopy.feedback = _feedback;
		}
		
		public function set length(length : int) : void {
			if ( channelCopy ) channelCopy.length = length;
			if ( _useMilliseconds ) {
				length = int( length * 44100 * 0.001 );
			}

			//prevents errors, allows dynamic change of _length
			if (_length != length)
			{
				var newBuffer : Vector.<Number> = new Vector.<Number>( length, true );
				if ( buffer ) newBuffer.concat( buffer );
				buffer = newBuffer;
				writePointer = 0;
			}
			_length = length;
		}
		
		public function get length() : int {
			return _length;
		}
		
		public function get mix() : Number {
			return _mix;
		}
		
		public function set mix(mix : Number) : void {
			_mix = mix;
			if ( channelCopy ) channelCopy.mix = _mix;
		}
		
		public function get useMilliseconds() : Boolean {
			return _useMilliseconds;
		}
		
		public function set useMilliseconds(useMilliseconds : Boolean) : void {
			useMilliseconds = useMilliseconds;
			if ( channelCopy ) channelCopy.useMilliseconds = useMilliseconds;
		}
	}
}
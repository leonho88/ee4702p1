// author : Leon Ho
package sound
{
	import builder.Canvas;
	import builder.CanvasAvatar;
	import builder.CanvasObject;
	import builder.CanvasSound;
	import builder.Material;
	
	import flash.events.SampleDataEvent;
	import flash.geom.Matrix3D;
	import flash.geom.Point;
	import flash.geom.Vector3D;
	import flash.media.Sound;
	import flash.media.SoundChannel;
	import flash.utils.ByteArray;
	import flash.utils.getTimer;
	
	import utils.CanvasUtils;
	import utils.IntersectionInfo;
	
	/*
	AudioListener
	An object owned by CanvasAvatar, which acts as the ears of the player in the world
	It extracts spatial and sound information from the world, compute their interactions according to defined rules, and outputs the sound
	related classes CanvasAvatar.as, Canvas.as, CanvasUtils.as, IntersectionInfo.as, Material.as
	*/
	public class AudioListener
	{
		private const SAMPLE_RATE:Number = 44100;
		private const CHUNK_LEN:uint = 3072;	
		private var halfpi:Number = 0.5*Math.PI;
		private var m_bufL:Vector.<Number> = new Vector.<Number>(CHUNK_LEN); // left buffer
		private var m_bufR:Vector.<Number> = new Vector.<Number>(CHUNK_LEN); // right buffer
		
		private var listenerGain:Number = 1.0;	// Gain [0,1]
		private var pan:Number = 0.0;		// Left-right pan [-1,1]
		
		private var outSound:Sound;
		private var outSoundChannel:SoundChannel;
		
		public var owner:CanvasAvatar;
		
		public var lag:delay;
		public var tempV3D:Vector3D;
		public var VtempV3D:Vector3D;
		public function AudioListener(owner:CanvasAvatar)
		{
			this.owner = owner;
			
			outSound = new Sound();
			outSound.addEventListener(SampleDataEvent.SAMPLE_DATA, onSampleData);
			outSoundChannel = outSound.play();
			lag = new delay();
			tempV3D = new Vector3D();
			VtempV3D = new Vector3D();
		}
		
		public function update(deltaTime:Number):void
		{
		}
		
		public function onSampleData(e:SampleDataEvent):void
		{
			// track latency, defined as time taken for writing the data to the audio output from the time this event was fired
			var startTime:int = getTimer();
			
			generateAudio(m_bufL, m_bufR, CHUNK_LEN); // generate the audio with physics simulation
			
			// write out the resulting audio
			for (var i:uint = 0; i < CHUNK_LEN; i++)
			{
				e.data.writeFloat(m_bufL[i]);
				e.data.writeFloat(m_bufR[i]);
			}
			
			Game.audioLatency = getTimer() - startTime;
		}
		
		public function generateAudio(outL:Vector.<Number>, outR:Vector.<Number>, n:uint):void
		{
			var numSounds:uint = owner.canvas.sounds.length;
			
			if (numSounds < 1)
			{
				// empty buffers
				m_bufL = new Vector.<Number>(CHUNK_LEN);
				m_bufR = new Vector.<Number>(CHUNK_LEN);
				return;
			}
			
			// temporary buffers
			var temp_bufL:Vector.<Number> = new Vector.<Number>(CHUNK_LEN);
			var temp_bufR:Vector.<Number> = new Vector.<Number>(CHUNK_LEN);
			
			//!! buffer for the echoes----
			var lag_bufL:Vector.<Number> = new Vector.<Number>(CHUNK_LEN);
			var lag_bufR:Vector.<Number> = new Vector.<Number>(CHUNK_LEN);
			//!!--------------------------

			// accumulator buffers
			var acc_bufL:Vector.<Number> = new Vector.<Number>(CHUNK_LEN);
			var acc_bufR:Vector.<Number> = new Vector.<Number>(CHUNK_LEN);
			
			var listenerPoint:Point = new Point(owner.attributes.position.x, owner.attributes.position.z);
			var sourcePoint:Point = new Point();
			
			for each (var snd:CanvasSound in owner.canvas.sounds)
			{
				var distance:Number;
				var audioSource:AudioSource = snd.audioSource;
				
				if (!audioSource.soundLoaded) return;
				
				// 1. EXTRACT SOUND DATA
				var byteArray:ByteArray = new ByteArray;
				
				// Try to grab n samples
				var len1:uint = audioSource.source.extract(byteArray, n);
				
				// If we couldn't get all n, wrap around to start of file
				if (snd.attributes.looping)
				{
					var len2:uint = n - len1;
					if (len2 > 0)
						len2 = audioSource.source.extract(byteArray, len2, 0);
				}
				
				// Rewind the byte array
				byteArray.position = 0;
				var i:int = 0;
				for (i = 0; i < n; i++)
				{
					if (!byteArray.bytesAvailable)
					{
						snd.attributes.active = 0;
						break;
					}
					temp_bufL[i] = byteArray.readFloat();
					temp_bufR[i] = byteArray.readFloat();
				}
				
				var vecToSource:Vector3D = Vector3D(snd.attributes.position).subtract(owner.attributes.position);
				
				// 2. COMPUTE INTERACTIONS
				sourcePoint.x = snd.attributes.position.x;
				sourcePoint.y = snd.attributes.position.z;
				var intersections:Vector.<IntersectionInfo> = CanvasUtils.castRay(sourcePoint, listenerPoint, owner.canvas.objects);
				
				if (intersections.length > 0)
				{	
					for each (var info:IntersectionInfo in intersections)
					{
						// SIMULATE OCCLUSION
						if (Canvas.occlusion)
						{
							var filter:EQ = Material.getTransmitFilterForMaterial(info.object.attributes.material);
							for (i = 0; i < n; i++)
							{
								temp_bufL[i] = filter.process(temp_bufL[i]);
								temp_bufR[i] = filter.process(temp_bufR[i]);
							}
						}
					}
				}
				// SIMULATE REVERB
				if(intersections.length == 0)
				{
					// authors : Arun, Chun Kit
					if (Canvas.reverb)
					{
						for each (var obj:CanvasObject in owner.canvas.objects)
						{
							//get the position of the virtual source by reflecting it on the wall
							var virtualPoint:Point = CanvasUtils.reflectPointInLine(sourcePoint, obj.start, obj.end);
							//trace ("virtualPointinAudioListener: " + virtualPoint);
							//cast ray to see if virtual ray instersects the wall
							var virtualRayIntersections:Vector.<IntersectionInfo> = CanvasUtils.castRay(listenerPoint, virtualPoint, owner.canvas.objects);
							//If it intersects there is a reflectin , therefore we do delay calculation
							if (virtualRayIntersections.length >=1 && obj == virtualRayIntersections[0].object)
							{
								//get the direction of the vector joining the virtual source and listner
								var lis2vir:Vector3D = CanvasUtils.direct(virtualPoint,listenerPoint);
								VtempV3D.x = -lis2vir.x;
								VtempV3D.y = lis2vir.z;
								VtempV3D.z = -lis2vir.y;

								//get the direction of the object (wall)
								var objdirection:Vector3D = CanvasUtils.direct(obj.end, obj.start);
								trace ("objdir: " + objdirection.x + " " + objdirection.y + " " + objdirection.z);

								var virtualdirection:Vector3D = CanvasUtils.reflect3D(objdirection, tempV3D);//snd.attributes.facing);
								trace ("snd.dir: " + snd.attributes.facing.x + " " + snd.attributes.facing.y + " " + snd.attributes.facing.z);

								trace ("virobjdir: " + virtualdirection.x + " " + virtualdirection.y + " " + virtualdirection.z);
								
								//culling of virtual sources based on direction
								var cull:Number = Vector3D.angleBetween(lis2vir,virtualdirection);
								trace ("cull: " + (cull*180/Math.PI));
								if(cull < Math.PI/180*snd.attributes.innerAngle) //angle between is acute so, virtual source contributes to reverb
								{
									//trace ("cull: " + cull);
									//get the distace from the listener to the virtual source
									distance = CanvasUtils.retDistance(listenerPoint, virtualPoint)*owner.canvas.ptm_ratio;
									//as distance increases the stregth of the feedback varies between 0.5 to 0.0(decreases)
									var feedback:Number = ((1.0-(distance/snd.attributes.max_distance))*0.2);
									//as distace increases the length of the delay sample that is heard decreases 
									var length:int = distance*3;
									var numTimes:Number = 1;
									//trace("feedback" + feedback);
									
									lag.feedback = feedback;
									lag.length = length*30;		//debug by ear
									trace("length" + lag.length);
									
									//!! distance attenuation, inverse distance rolloff model, clamped for virtual sources----------
									//converts back to Leon convention
									tempV3D.x = -snd.attributes.facing.x;
									tempV3D.y = -snd.attributes.facing.z;
									tempV3D.z= snd.attributes.facing.y;
									distance = Math.max(distance, snd.attributes.reference_distance); 
									distance = Math.min(distance, snd.attributes.max_distance);
									listenerGain = snd.attributes.reference_distance / (snd.attributes.reference_distance + snd.attributes.rolloff_factor * (distance - snd.attributes.reference_distance));
									listenerGain *= snd.attributes.sourceGain;
									trace("listenerGain: " + listenerGain);
									
									// Panning of virtual source-----
									var RotMat:Matrix3D = new Matrix3D();
									RotMat.appendRotation(-90, Vector3D.Y_AXIS);
									var RightSideVec:Vector3D = RotMat.transformVector(owner.attributes.facing);
									RightSideVec.normalize();
									var Angle:Number =  Vector3D.angleBetween(RightSideVec, VtempV3D);
									pan = (halfpi - Angle) / halfpi;
									
									for (i = 0; i < n; i++)
									{
										//CK can you help me fix the bug? I think i am doing somethign wrong while calling process in filter
										//processes temp buffer
										//problem fixed under delay.as
										lag_bufL[i] = lag.process(temp_bufL[i],numTimes)*listenerGain*Math.min(1, 1-pan);
										lag_bufR[i] = lag.process(temp_bufR[i],numTimes)*listenerGain*Math.max(0, 1+pan);
									}									
								}
								else
								{
									//trace ("cull: " + cull);
								}//angle between is obtuse so, virtual source doesnt contribute to reverb
							}
						}
					}
				}
				
				// 3. ATTENUATE
				// cone attenuation co-author: Arun, Chun Kit
				var gain_multiplier:Number;
				var vecFromSource:Vector3D = vecToSource.clone();
				vecFromSource.negate();
				var innerAngleRad:Number = (snd.attributes.innerAngle/180)*Math.PI;
				var outerAngleRad:Number = (snd.attributes.outerAngle/180)*Math.PI;
				var coneAngle:Number = Vector3D.angleBetween(vecFromSource, snd.attributes.facing);
				// within inner cone
				if (coneAngle <= innerAngleRad*0.5)
					gain_multiplier = 1.0;
				else if (coneAngle >= outerAngleRad*0.5)
					gain_multiplier = 0.1;
				else
					gain_multiplier = ((outerAngleRad*0.5) - coneAngle)/(outerAngleRad*0.5 - innerAngleRad*0.5) + 0.1;
				
				// distance attenuation, exponential distance, clamped
				var ptm_ratio:Number = owner.canvas.ptm_ratio;
				var dist:Number = vecToSource.length*ptm_ratio;
				var ref_dist:Number = snd.attributes.reference_distance;
				var max_dist:Number = snd.attributes.max_distance;
				dist = Math.max(dist, ref_dist); 
				dist = Math.min(dist, max_dist);
				if (dist >= max_dist) listenerGain = 0.0;
				else if (dist <= ref_dist) listenerGain = 1.0;
				else listenerGain = Math.pow((dist / ref_dist), -snd.attributes.rolloff_factor);
				//listenerGain = ref_dist / (ref_dist + snd.attributes.rolloff_factor * (dist - ref_dist));
				listenerGain *= snd.attributes.sourceGain; // result multiplied by source gain
				
				// 4. PAN
				var halfpi:Number = 0.5*Math.PI;
				var rotMat:Matrix3D = new Matrix3D();
				rotMat.appendRotation(-90, Vector3D.Y_AXIS);
				var rightSideVec:Vector3D = rotMat.transformVector(owner.attributes.facing);
				rightSideVec.normalize();
				var angle:Number =  Vector3D.angleBetween(rightSideVec, vecToSource);
				pan = (halfpi - angle) / halfpi;
				
				// 5. MIX AND WRITE INTO ACCUMULATOR BUFFERS
				// Combine gain and pan into net gain for each channel
				var gainL:Number = listenerGain * Math.min(1, 1-pan) * gain_multiplier;
				var gainR:Number = listenerGain * Math.max(0, 1+pan) * gain_multiplier;

				// Copy the audio data into acc buffers
				var mixFactor:Number = 1.0 / numSounds;
				var reverbEQ:EQ = Material.getReverbFilterForMaterial(obj.attributes.material)
				for (i = 0; i < n; i++)
				{
					acc_bufL[i] += mixFactor*gainL*temp_bufL[i] + lag_bufL[i];
					acc_bufR[i] += mixFactor*gainR*temp_bufR[i] + lag_bufR[i];
				}
			}
			// 6. WRITE DATA TO OUTPUT BUFFERS
			for (i = 0; i < n; i++)
			{
				// scale back the gain
				m_bufL[i] = acc_bufL[i] * numSounds;
				m_bufR[i] = acc_bufR[i] * numSounds;
			}
		}
	}
}
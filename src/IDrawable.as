// author : Leon Ho
package
{
	public interface IDrawable
	{
		function draw(deltaTime:Number):void;
	}
}
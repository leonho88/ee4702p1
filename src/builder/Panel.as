// author : Leon Ho
package builder
{	
	import flash.display.DisplayObjectContainer;
	import flash.display.Sprite;
	import flash.events.KeyboardEvent;
	import flash.text.TextField;
	import flash.text.TextFieldAutoSize;
	import flash.text.TextFieldType;
	import flash.text.TextFormat;
	import flash.ui.Keyboard;
	import flash.utils.Dictionary;
	
	/*
	Panel
	A panel similar to object explorers in many other applications that allows inspection and modification of an object's attributes
	related classes ICanvasObject.as
	*/
	public class Panel extends Sprite implements IUpdateable, IDrawable
	{
		private var panelTextFormat:TextFormat;

		private var selectionAttributePanel:Sprite;
		private var canvasAttributePanel:Sprite;
		
		private var canvas:Canvas;
		
		private var currentCanvasObject:ICanvasObject = null;
		
		private var binding:Dictionary = new Dictionary();
		
		private const padding:uint = 3;
		
		public function Panel(canvas:Canvas)
		{
			super();
			
			this.canvas = canvas;
			
			panelTextFormat = new TextFormat("Helvetica", 12);
			
			selectionAttributePanel = new Sprite();
			addChild(selectionAttributePanel);
			selectionAttributePanel.addEventListener(KeyboardEvent.KEY_UP, onKeyUp);
			
			canvasAttributePanel = new Sprite();
			addChild(canvasAttributePanel);
			canvasAttributePanel.y = 400;
			canvasAttributePanel.addEventListener(KeyboardEvent.KEY_UP, onKeyUp);
			addFieldsForPanel(canvasAttributePanel, canvas.attributes);
		}
		 
		private function addFieldsForPanel(panel:DisplayObjectContainer, obj:Object):void
		{
			for (var attr:String in obj)
			{
				var label:TextField = new TextField();
				label.defaultTextFormat = panelTextFormat;
				label.selectable = false;
				label.text = attr;
				label.autoSize = TextFieldAutoSize.RIGHT;
				panel.addChild(label);
				
				label.y = panel.numChildren*(label.textHeight + padding);
				
				var field:TextField = new TextField();
				field.type = TextFieldType.INPUT;
				field.border = true;
				field.defaultTextFormat = panelTextFormat;
				field.text = "" + obj[attr];
				field.autoSize = TextFieldAutoSize.NONE;
				field.height = field.textHeight + padding;
				panel.addChild(field);
				
				field.x = label.x + label.width + padding;
				field.y = label.y;
				
				binding[field] = attr;
			}
		}
		
		private function removeFields():void
		{
			selectionAttributePanel.removeChildren();
		}
		
		private function onKeyUp(e:KeyboardEvent):void
		{
			if (e.keyCode == Keyboard.ENTER)
			{
				var field:TextField = e.target as TextField;
				var boundAttribute:String = binding[field];
				var value:Number = Number(field.text);
				if (isNaN(value))
					Game.console.log(boundAttribute + " cannot be set.");
				if (e.currentTarget == selectionAttributePanel)
				{
					currentCanvasObject.attributes[boundAttribute] = value;
				}
				else
				{
					canvas.attributes[boundAttribute] = value;
				}
			}
		}
		
		public function update(deltaTime:Number):void
		{
			if (currentCanvasObject != canvas.selectedObject)
			{
				removeFields();
				currentCanvasObject = canvas.selectedObject;
				if (currentCanvasObject)
					addFieldsForPanel(selectionAttributePanel, currentCanvasObject.attributes);
			}
		}
		
		public function draw(deltaTime:Number):void
		{
			
		}
	}
}
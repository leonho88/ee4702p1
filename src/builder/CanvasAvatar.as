package builder
{
	import flash.display.Sprite;
	import flash.events.KeyboardEvent;
	import flash.events.MouseEvent;
	import flash.geom.Vector3D;
	import flash.ui.Keyboard;
	
	import sound.AudioListener;
	/*
	CanvasAvatar
	Our game avatar, the player character, is also our only listener in the world
	related classes AudioListener.as
	*/
	public class CanvasAvatar extends Sprite implements IDrawable, IUpdateable, ICanvasObject
	{
		private var _selected:Boolean = false;
		public function get selected():Boolean
		{
			return _selected;
		}
		public function set selected(val:Boolean):void
		{
			_selected = val;
		}
		
		private var _attributes:Object = new Object();
		public function get attributes():Object
		{
			return _attributes;
		}
		
		private var move:int = 0;
		
		public var canvas:Canvas;
		
		public var audioListener:AudioListener;
		
		public function CanvasAvatar(canvas:Canvas)
		{
			super();
			
			this.canvas = canvas;
			
			_attributes.position = new Vector3D();
			_attributes.facing = new Vector3D(0, 0, 1);
			_attributes.maxSpeed = 1.4; // average human walking speed
			
			audioListener = new AudioListener(this);
			
			this.addEventListener(KeyboardEvent.KEY_UP, onKeyUp);
		}
		
		private function onKeyUp(e:KeyboardEvent):void
		{
			if (e.keyCode == Keyboard.ESCAPE)
			{
				// deselect
				canvas.selectedObject = null;
			}
		}
		
		public function rotateTo(x:int, z:int):void
		{
			var to:Vector3D = new Vector3D(x, 0, z);
			_attributes.facing = to.subtract(_attributes.position);
			(_attributes.facing as Vector3D).normalize();
		}
		
		public function moveForward():void
		{
			move = 1;
		}
		
		public function moveBackward():void
		{
			move = -1;
		}
		
		public function stop():void
		{
			move = 0;
		}
		
		public function update(deltaTime:Number):void
		{
			// move the avatar
			x += move * _attributes.facing.x * _attributes.maxSpeed * 1/canvas.ptm_ratio * deltaTime;
			y += move * _attributes.facing.z * _attributes.maxSpeed * 1/canvas.ptm_ratio * deltaTime;
			
			// updates the avatar's position if user drags it around
			_attributes.position.x = this.x;
			_attributes.position.z = this.y;
			
			audioListener.update(deltaTime);
		}
		
		public function draw(deltaTime:Number):void
		{
			graphics.clear();
			var radius:int = 8;
			graphics.lineStyle(0);
			graphics.beginFill(0x0000FF);
			graphics.drawCircle(0, 0, radius);
			graphics.endFill();
			graphics.lineStyle(3, 0xFFFF00);
			graphics.moveTo(0, 0);
			graphics.lineTo(_attributes.facing.x*radius, _attributes.facing.z*radius);
		}
	}
}
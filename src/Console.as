// author : Leon Ho
package
{
	import builder.Canvas;
	import builder.CanvasFile;
	
	import flash.display.Sprite;
	import flash.events.KeyboardEvent;
	import flash.geom.Point;
	import flash.text.TextField;
	import flash.text.TextFieldAutoSize;
	import flash.text.TextFieldType;
	import flash.text.TextFormat;
	import flash.ui.Keyboard;
	import flash.utils.Dictionary;
	
	public class Console extends Sprite implements IUpdateable
	{
		private const consoleHotKey:uint = 192;
		private var consoleToggle:Boolean = false;
		
		private var input:TextField = new TextField;
		private var output:TextField = new TextField;
		
		private var stats:TextField = new TextField;
		
		private var game:Game;
		
		private var commands:Dictionary = new Dictionary;
		
		private var history:Vector.<String> = new Vector.<String>;
		private const MAX_HISTORY:uint = 5;
		private var historyIndex:int = 0;
		
		public function Console(game:Game)
		{
			super();
			
			this.game = game;
			
			var format:TextFormat = new TextFormat("Helvetica", 12, 0x000000, true);
			
			input.defaultTextFormat = format;
			input.type = TextFieldType.INPUT;
			input.autoSize = TextFieldAutoSize.NONE;
			input.background = true;
			input.backgroundColor = 0x696969;
			input.width = game.stage.stageWidth;
			input.height = 20;
			input.y = 68;//game.stage.stageHeight*0.3 - input.height;
			addChild(input);
			
			output.defaultTextFormat = format;
			output.type = TextFieldType.DYNAMIC;
			output.autoSize = TextFieldAutoSize.NONE;
			output.background = true;
			output.backgroundColor = 0x696969;
			output.multiline = true;
			output.width = game.stage.stageWidth * 0.5;
			output.height = 68;//game.stage.stageHeight*0.2 - input.height;
			output.selectable = false;
			output.mouseWheelEnabled = true;
			output.alpha = 0.8;
			addChild(output);
			
			stats.defaultTextFormat = format;
			stats.type = TextFieldType.DYNAMIC;
			stats.autoSize = TextFieldAutoSize.NONE;
			stats.background = true;
			stats.backgroundColor = 0x696969;
			stats.multiline = true;
			stats.width = game.stage.stageWidth * 0.5;
			stats.x = output.width;
			stats.height = 68;//game.stage.stageHeight*0.2 - input.height;
			stats.selectable = false;
			stats.alpha = 0.8;
			addChild(stats);
			
			this.y = game.stage.stageHeight - this.height;
			
			game.stage.addEventListener(KeyboardEvent.KEY_UP, onKeyUp);
			
			// clear the console
			addCommand("clear", "clear");
			// list the available commands
			addCommand("list", "list");
			
			/*
			addCommand("run", "run");
			addCommand("stop", "stop");
			*/
			addCommand("grid", "grid");
			addCommand("debug", "debug");
			addCommand("reverb", "reverb");
			addCommand("occlusion", "occlusion");
			
			addCommand("addsound", "addSound");
			
			addCommand("save", "save");
			addCommand("load", "load");
			
			/*
			addCommand("gettilesize", "getTileSize");
			// sets width and height of tile
			addCommand("settilesize", "setTileSize");
			addCommand("getworldsize", "getWorldSize");
			// sets number of tiles in x and y 
			addCommand("setworldsize", "setWorldSize");
			// adds tile(s) at x y with w h with material m
			addCommand("addtile", "addTile");
			addCommand("removetile", "removeTile");
			
			// adds a sound emitter in tile x y
			addCommand("addemitter", "addEmitter");
			
			addCommand("getspeed", "getSpeed");
			addCommand("setspeed", "setSpeed");
			*/
		}
		
		public function log(str:String):void
		{
			output.appendText(str + "\n");
			output.scrollV = output.maxScrollV;
		}
		
		public function onKeyUp(e:KeyboardEvent):void
		{
			if (e.keyCode == consoleHotKey)
			{
				input.text = "";
				consoleToggle = !consoleToggle;
				if (consoleToggle)
				{
					game.addChild(this);
					// grab focus
					stage.focus = input;
				}
				else
				{
					// return focus
					stage.focus = stage;
					game.removeChild(this);
				}
			}
			else if (e.keyCode == Keyboard.UP)
			{
				if (history.length > 0)
				{
					input.text = history[historyIndex];
					input.setSelection(input.length, input.length);
					historyIndex = Math.max(historyIndex - 1, 0);
				}
			}
			else if (e.keyCode == Keyboard.DOWN)
			{
				if (history.length > 0)
				{
					if (historyIndex == history.length - 1)
					{
						input.text = "";
					}
					else
					{
						historyIndex = Math.min(historyIndex + 1, history.length - 1);
						input.text = history[historyIndex];
						input.setSelection(input.length, input.length);
					}
				}
			}
			else if (e.keyCode == Keyboard.ENTER)
			{
				var text:String = input.text.toLowerCase();
				var line:Array = text.split(" ");

				var outputText:String;
				if (commands[line[0]])
				{
					if (history.length == MAX_HISTORY)
						history.shift();
					history.push(commands[line[0]]);
					historyIndex = history.length - 1;
					
					var command:Function = this[commands[line[0]]] as Function;
					var args:Array = line.slice(1);
					outputText = processCommand(command, args);
				}
				else
				{
					outputText = "no such command";
				}
				output.appendText(outputText + "\n");
				output.scrollV = output.maxScrollV;
				input.text = "";
			}
		}
		
		public function processCommand(command:Function, args:Array):String
		{
			var out:String;
			try 
			{
				switch (args.length)
				{
					case 0:
						out = command();
						break;
					case 1:
						out = command(args[0]);
						break;
					case 2:
						out = command(args[0], args[1]);
						break;
					case 3:
						out = command(args[0], args[1], args[2]);
						break;
					case 4:
						out = command(args[0], args[1], args[2], args[3]);
						break;
					case 5:
						out = command(args[0], args[1], args[2], args[3], args[4]);
						break;
				}
			}
			catch (e:Error)
			{
				out = "incorrect function call";
			}
			return out;
		}
		
		public function addCommand(command:String, commandHandler:String):void
		{
			commands[command] = commandHandler;
		}
		
		public function update(deltaTime:Number):void
		{
			stats.text = 	"fps: " + Game.fps + "\n" + 
							"audioLatency: " + Game.audioLatency + "ms \n";
		}
		
		public function clear():String
		{
			output.text = "";
			return "";
		}
		
		public function list():String
		{
			var cmds:Array = new Array;
			for (var key:Object in commands)
			{
				cmds.push(key);
			}
			return cmds.join("\n");
		}

		public function grid(onOrOff:int):String
		{
			Canvas.grid = onOrOff ? true : false;
			
			return onOrOff ? "grid on" : "grid off";
		}
		
		public function debug(onOrOff:int):String
		{
			Canvas.debug = onOrOff ? true : false;
			
			return onOrOff ? "debug on" : "debug off";
		}
		
		public function reverb(onOrOff:int):String
		{
			Canvas.reverb = onOrOff ? true : false;
			
			return onOrOff ? "reverb on" : "reverb off";
		}
		
		public function occlusion(onOrOff:int):String
		{
			Canvas.occlusion = onOrOff ? true : false;
			
			return onOrOff ? "occlusion on" : "occlusion off";
		} 
		
		public function addSound(x:int, y:int, file:String):String
		{
			Game.builder.canvas.addCanvasSound(new Point(x, y), file);
			
			return "sound added";
		}
		
		public function save():String
		{
			CanvasFile.save();
			
			return "saving";
		}
		
		public function load():String
		{
			CanvasFile.load();
			
			return "loading";
		}
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		/*
		public function getTileSize():String
		{
			return "tileSize=" + game.world.tileWidth + "x" + game.world.tileHeight;
		}
		
		public function setTileSize(w:uint, h:uint):String
		{
			game.world.changeTileSize(w, h);
			
			return "tileSize=" + w + "x" + h;
		}
		
		public function getWorldSize():String
		{
			return "worldSize=" + game.world.columns + "x" + game.world.rows;
		}
		
		public function setWorldSize(w:uint, h:uint):String
		{
			game.world.changeWorldSize(w, h);
			
			return "worldSize = " + w + "x" + h;
		}
		
		public function addTile(x:uint, y:uint, w:uint, h:uint, m:String = Material.NONE):String
		{
			game.world.addTile(x, y, w, h, m);
			
			return "tile of size " + w + " by " + h + " with material " + m + " added at (" + x + ", " + y + ")";
		}
		
		public function removeTile(x:uint, y:uint, w:uint, h:uint):String
		{
			game.world.removeTile(x, y, w, h);
			
			return "tiles removed";
		}
		
		public function addEmitter(x:uint, y:uint, file:String):String
		{
			game.world.addEmitter(x, y, file);
				
			return "emitter added at (" + x + ", " + y + "), playing " + file;
		}
		
		public function getSpeed():String
		{
			return "avatar speed is " + game.world.avatar.speed + " px/s";
		}
		
		public function setSpeed(val:Number):String
		{
			game.world.avatar.speed = val;
			
			return "avatar speed set to " + val + " px/s";
		}
		*/
	}
}